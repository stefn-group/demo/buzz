package fizz.buzz.service.impl;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BuzzServiceImplTests {

	@Autowired
	BuzzServiceImpl buzzServiceImpl;

	@Test
	public void testApplyFunctions() {
		final int[] input = new int[] { 1, 3, 4, 5, 6, 7, 10, 15, 21, 35, 105 };
		final List<String> res = buzzServiceImpl.applyFunctions(input);
		final String[] expected = new String[] { "1", "fizz", "4", "buzz", "fizz", "Bazz", "buzz", "fizzbuzz",
				"FizzBazz", "BuzzBazz", "FizzBuzzBazz" };

		assertArrayEquals(expected, res.toArray(), "lists must to match!");
	}

}
