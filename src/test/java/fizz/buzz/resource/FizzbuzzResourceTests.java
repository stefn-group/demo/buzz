package fizz.buzz.resource;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import fizz.buzz.BuzzApplication;

@SpringBootTest(classes = BuzzApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FizzbuzzResourceTests {

	@LocalServerPort
	private int port;

	TestRestTemplate restTemplate = new TestRestTemplate();

	HttpHeaders headers = new HttpHeaders();

	@Test
	public void testGet() {

		final HttpEntity<String> entity = new HttpEntity<>(null, headers);

		final ResponseEntity<String> response = restTemplate
				.exchange(createURLWithPort("/api/v1/fizzbuzz?entry=-3 5 7 105"), HttpMethod.GET, entity, String.class);

		final String expected = "{\"value\":[\"fizz\",\"buzz\",\"Bazz\",\"FizzBuzzBazz\"]}";

		assertEquals(expected, response.getBody(), "Expected response was not received");
	}

	@Test
	public void testGetWithWrongParam() {

		final HttpEntity<String> entity = new HttpEntity<>(null, headers);
		final ResponseEntity<String> response = restTemplate
				.exchange(createURLWithPort("/api/v1/fizzbuzz?entry=3 q 7 105"), HttpMethod.GET, entity, String.class);
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode(),
				"Wrong status received for bad input.");
	}

	@Test
	public void testDelete() {

		final HttpEntity<String> entity = new HttpEntity<>(null, headers);
		final ResponseEntity<String> response = restTemplate.exchange(createURLWithPort("/api/v1/fizzbuzz"),
				HttpMethod.DELETE, entity, String.class);
		assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode(), "Wrong status method not allowed.");
	}

	@Test
	public void testPost() {

		final HttpEntity<String> entity = new HttpEntity<>(null, headers);
		final ResponseEntity<String> response = restTemplate
				.exchange(createURLWithPort("/api/v1/fizzbuzz?entry=3 q 7 105"), HttpMethod.POST, entity, String.class);
		assertEquals(HttpStatus.METHOD_NOT_ALLOWED, response.getStatusCode(), "Wrong status method not allowed.");
	}

	private String createURLWithPort(final String uri) {
		return "http://localhost:" + port + uri;
	}
}
