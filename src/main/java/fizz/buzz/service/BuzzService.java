package fizz.buzz.service;

import java.util.List;

/**
 * Service used to handle the chained functions needed in order to process the input.
 * @author narcis
 *
 */
public interface BuzzService {

	/**
	 * Apply the functions chain.
	 * @param inputs inputs as array of int
	 * @return list of responses, one for each argument
	 */
	List<String> applyFunctions(int[] inputs);
}
