package fizz.buzz.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fizz.buzz.function.Function;
import fizz.buzz.resource.FizzbuzzResource;
import fizz.buzz.service.BuzzService;

/**
 * Impl for the service using a bit map in order to recover the corresponding
 * values. Entries are stored as sum of power of 2
 *
 * @author narcis
 *
 */
@Service
public class BuzzServiceImpl implements BuzzService {

	private final static Logger LOGGER = LoggerFactory.getLogger(FizzbuzzResource.class);

	@SuppressWarnings("serial")
	static final Map<Integer, String> REPLACEMENT_MAP = new HashMap<Integer, String>() {

		{
			put(1 << 1, "fizz");// 2
			put(1 << 2, "buzz");// 4
			put(1 << 3, "Bazz");// 8
			put((1 << 1) + (1 << 2), "fizzbuzz");// 6
			put((1 << 1) + (1 << 3), "FizzBazz");// 10
			put((1 << 2) + (1 << 3), "BuzzBazz");// 12
			put((1 << 1) + (1 << 2) + (1 << 3), "FizzBuzzBazz");// 14
		}
	};

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<String> applyFunctions(final int[] inputs) {
		LOGGER.debug("applyFunctions start for [{}]", inputs);

		final Function f = new Function() {
		};

		final List<String> result = Arrays.stream(inputs).mapToObj(elem -> {
			int r = f.apply(elem, 1);
			r += f.apply(elem, 2);
			r += f.apply(elem, 3);
			final Integer res = Integer.valueOf(r);

			return REPLACEMENT_MAP.containsKey(res) ? REPLACEMENT_MAP.get(res) : String.valueOf(elem);
		}).collect(Collectors.toList());

		LOGGER.debug("applyFunctions end for [{}]", result);

		return result;
	}

}
