package fizz.buzz.function;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Function used in orde to handle the input.
 *
 * @author narcis
 *
 */
public interface Function extends BiFunction<Integer, Integer, Integer> {

	final static Logger LOGGER = LoggerFactory.getLogger(Function.class);

	@SuppressWarnings("serial")
	static final Map<Integer, Integer> CORRESPONDENCE_MAP = new HashMap<Integer, Integer>() {
		{
			put(1, 3);
			put(2, 5);
			put(3, 7);
		}
	};

	/**
	 * Compute the value for the argument as 2^pos if argument is multiple of pos
	 * corresponding value as: pos1 -> 3, pos2 -> 5, pos3 -> 7
	 *
	 * @param arg the value to be checked
	 * @param pos 1-based index of the checked value
	 * @return 2^pos if argument is a multiple of the given value, or 0 otherwise
	 */
	@Override
	default Integer apply(final Integer arg, final Integer pos) {
		LOGGER.debug("apply start for [{}] pos {}", arg, pos);

		final int result = (arg.intValue() % CORRESPONDENCE_MAP.get(pos.intValue())) == 0 ? 1 << pos : 0;

		LOGGER.debug("apply end for [{}] pos {}: ", arg, pos);
		return result;
	}

}
