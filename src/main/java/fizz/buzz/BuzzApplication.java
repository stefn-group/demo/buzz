package fizz.buzz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main Spring Boot Application class.
 *
 * @author narcis
 *
 */
@SpringBootApplication
public class BuzzApplication {

	public static void main(final String[] args) {
		SpringApplication.run(BuzzApplication.class, args);
	}

}
