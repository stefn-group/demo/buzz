package fizz.buzz.resource;

import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import fizz.buzz.model.Result;
import fizz.buzz.service.BuzzService;

@Component
@Path("/fizzbuzz")
public class FizzbuzzResource {

	private final static Logger LOGGER = LoggerFactory.getLogger(FizzbuzzResource.class);

	@Autowired
	BuzzService buzzService;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Result getResultForEntry(@QueryParam("entry") String entryArray) {
		LOGGER.debug("getResultForEntry start with [{}]", entryArray);

		if (!StringUtils.hasText(entryArray)) {
			final StringBuffer sb = new StringBuffer();
			for (int i = 0; i < 100; i++) {
				sb.append(i + 1).append(" ");
			}
			entryArray = sb.deleteCharAt(sb.length() - 1).toString();
		}

		int[] array;
		try {
			array = preProcessInput(entryArray);
		} catch (final NumberFormatException e) {
			LOGGER.error("Non numerinc input!", e);
			throw new InternalServerErrorException("Can't parse as number!");
		}
		final Result result = new Result();
		result.setValue(buzzService.applyFunctions(array));
		LOGGER.debug("getResultForEntry end with [{}]", array);

		return result;
	}

	private int[] preProcessInput(final String input) throws NumberFormatException {

		final String[] entries = input.split(" ");
		final int[] result = new int[entries.length];
		for (int i = 0; i < entries.length; i++) {
			result[i] = Integer.valueOf(entries[i]);
		}
		return result;
	}
}
