package fizz.buzz.model;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Model used to represent the response.
 * @author narcis
 *
 */
public class Result implements Serializable {

	private static final long serialVersionUID = -7783894258894648869L;
	
	private List<String> value;

	/**
	 * @return the value
	 */
	public List<String> getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(List<String> value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Result)) {
			return false;
		}
		Result other = (Result) obj;
		return Objects.equals(value, other.value);
	}

	@Override
	public String toString() {
		return "Result [value=" + value + "]";
	}


	
	
}
