package fizz.buzz;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import fizz.buzz.resource.FizzbuzzResource;

@Configuration
@ApplicationPath("api/v1")
public class RestConfig extends ResourceConfig {

	@PostConstruct
	public void init() {
		register(FizzbuzzResource.class);
	}

}
